# database.py
from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
import os
import logging
import asyncio

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Update the DATABASE_URL with your MySQL connection details
DATABASE_URL = os.getenv("DATABASE_URL", "mysql+aiomysql://root:mastertai0125@mysql-service:3306/JuboTest")

engine = create_async_engine(DATABASE_URL, echo=True)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

Base = declarative_base()

class MyItem(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String(255), index=True)
    is_done = Column(Boolean, default=False)

async def create_tables():
    retries = 5
    for i in range(retries):
        try:
            async with engine.begin() as conn:
                await conn.run_sync(Base.metadata.create_all)
            logger.info("Tables created successfully")
            break
        except Exception as e:
            logger.error(f"Error creating tables: {e}, retrying in 5 seconds...")
            await asyncio.sleep(5)
